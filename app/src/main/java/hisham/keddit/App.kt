package hisham.keddit

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import hisham.keddit.di.AppComponent
import hisham.keddit.di.AppModule
import hisham.keddit.di.DaggerAppComponent

class App : Application() {

	companion object {
		@JvmStatic fun getAppComponent(context: Activity): AppComponent
			= (context.application as App).appComponent

		@JvmStatic fun getAppComponent(fragment: Fragment): AppComponent
			= (fragment.activity.application as App).appComponent
	}

	val appComponent: AppComponent by lazy {
		DaggerAppComponent
			.builder()
			.appModule(AppModule(applicationContext))
			.build()
	}

	override fun onCreate() {
		super.onCreate()

	}
}