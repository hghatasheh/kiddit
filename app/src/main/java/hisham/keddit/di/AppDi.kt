package hisham.keddit.di

import android.content.Context
import dagger.Component
import dagger.Module
import dagger.Provides
import hisham.keddit.network.RestClient
import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class AppScope

@AppScope
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

	fun context(): Context

	fun restClient(): RestClient
}

@Module
class AppModule(val context: Context) {

	@Provides
	@AppScope
	fun context(): Context = context

	@Provides
	@AppScope
	fun restClient(): RestClient = RestClient()
}