package hisham.keddit.common

class LoggingUtils {

	companion object {
		fun l(func: () -> Unit) {
			func()
		}

		fun l(tag: String, func: () -> Unit) {
			func()
		}
	}
}
