package hisham.keddit.common.ui

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import hisham.keddit.R
import hisham.keddit.base.AdapterViewType
import hisham.keddit.base.ViewTypeDelegateAdapter
import hisham.keddit.common.ext.inflate

class LoadingDelegateAdapter : ViewTypeDelegateAdapter {

	override fun onCreateViewHolder(parent: ViewGroup) = LoadingViewHolder(parent)

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder, itemType: AdapterViewType) {
		// NO-OP
	}

	inner class LoadingViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
		parent.inflate(R.layout.view_loading)
	)
}