@file:JvmName("ExtUtils")

package hisham.keddit.common.ext

import android.os.Parcel
import android.os.Parcelable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import hisham.keddit.R

@JvmOverloads // will generate two methods in this case, the first one with layoutId only and the second one with layoutId and attachToRoot
fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
	return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

fun ImageView.load(url: String?) {
	if (TextUtils.isEmpty(url)) {
		Picasso.with(context)
			.load(R.mipmap.ic_launcher)
			.into(this)
	} else {
		Picasso.with(context)
			.load(url)
			.into(this)
	}
}

inline fun <reified T : Parcelable> createParcel(
	crossinline fromParcel: (Parcel) -> T): Parcelable.Creator<T> {
	return object : Parcelable.Creator<T> {
		override fun createFromParcel(source: Parcel): T = fromParcel(source)
		override fun newArray(size: Int): Array<out T?> = arrayOfNulls(size)
	}
}