package hisham.keddit.news.models

import android.os.Parcel
import android.os.Parcelable
import hisham.keddit.base.AdapterViewType
import hisham.keddit.base.Constants
import hisham.keddit.common.ext.createParcel

data class RedditNews(val after: String,
											val before: String,
											val news: List<RedditNewsItem>) : Parcelable {

	companion object {
		@JvmField val CREATOR = createParcel { RedditNews(it) }
	}

	protected constructor(source: Parcel) : this(
		source.readString(),
		source.readString(),
		mutableListOf<RedditNewsItem>().apply {
			source.readTypedList(this, RedditNewsItem.CREATOR)
		}
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel?, flags: Int) {
		dest?.writeString(after)
		dest?.writeString(before)
		dest?.writeTypedList(news)
	}

	data class RedditNewsItem(
		val author: String,
		val title: String,
		val numComments: Int,
		val created: Long,
		val thumbnail: String,
		val url: String?
	) : Parcelable, AdapterViewType {

		override fun getViewType() = Constants.VIEW_NEWS

		companion object {
			@JvmField val CREATOR = createParcel { RedditNewsItem(it) }
		}

		constructor(source: Parcel) : this(
			source.readString(),
			source.readString(),
			source.readInt(),
			source.readLong(),
			source.readString(),
			source.readString())

		override fun describeContents() = 0

		override fun writeToParcel(dest: Parcel?, flags: Int) {
			dest?.writeString(author)
			dest?.writeString(title)
			dest?.writeInt(numComments)
			dest?.writeLong(created)
			dest?.writeString(thumbnail)
			dest?.writeString(url)
		}
	}
}

