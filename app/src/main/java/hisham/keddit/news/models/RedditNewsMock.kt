package hisham.keddit.news.models

import java.util.*

class RedditNewsMock {

	companion object {
		fun mocks(): ArrayList<RedditNews.RedditNewsItem> {
			val list: ArrayList<RedditNews.RedditNewsItem> = (1..10).mapTo(ArrayList()) {
				RedditNews.RedditNewsItem("Author: $it",
					"Title: $it",
					it,
					System.currentTimeMillis(),
					"http://lorempixel.com/200/200/technics/$it",
					null)
			}

			return list
		}
	}
}

