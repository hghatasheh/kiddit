package hisham.keddit.news.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import hisham.keddit.base.AdapterViewType
import hisham.keddit.base.Constants
import hisham.keddit.base.ViewTypeDelegateAdapter
import hisham.keddit.common.ui.LoadingDelegateAdapter
import hisham.keddit.news.models.RedditNews
import java.util.*

class NewsAdapter(itemSelectedListener: NewsDelegateAdapter.OnItemSelectedListener)
	: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

	private var items: ArrayList<AdapterViewType>
	private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()

	private val loadingItem = object : AdapterViewType {
		override fun getViewType() = Constants.VIEW_LOADING
	}

	init {
		delegateAdapters.put(Constants.VIEW_LOADING, LoadingDelegateAdapter())
		delegateAdapters.put(Constants.VIEW_NEWS, NewsDelegateAdapter(itemSelectedListener))

		items = ArrayList()
		items.add(loadingItem)
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
		return delegateAdapters.get(viewType).onCreateViewHolder(parent)
	}

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
		delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items[position])
	}

	override fun getItemCount(): Int {
		return items.size
	}

	override fun getItemViewType(position: Int): Int {
		return items[position].getViewType()
	}

	fun addNews(news: List<RedditNews.RedditNewsItem>) {
		val loadingPosition = items.size - 1

		removeLoadingView()
		items.addAll(news)
		items.add(loadingItem)
		notifyItemRangeChanged(loadingPosition, itemCount + 1 /* loading item */)
	}

	fun setNews(news: List<RedditNews.RedditNewsItem>) {
		items.clear()
		notifyItemRangeRemoved(0, getLastPosition())

		items.addAll(news)
		items.add(loadingItem)
		notifyItemRangeInserted(0, items.size)
	}

	fun removeLoadingView() {
		val loadingPosition = itemCount- 1
		items.removeAt(loadingPosition)
		notifyItemRemoved(loadingPosition)
	}

	fun getNews(): List<RedditNews.RedditNewsItem> {
		return items
			.filter { it.getViewType() == Constants.VIEW_NEWS }
			.map { it as RedditNews.RedditNewsItem }
	}

	private fun getLastPosition() = if (items.lastIndex == -1) 0 else items.lastIndex
}