package hisham.keddit.news.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import hisham.keddit.R
import hisham.keddit.base.AdapterViewType
import hisham.keddit.base.ViewTypeDelegateAdapter
import hisham.keddit.common.ext.getFriendlyTime
import hisham.keddit.common.ext.inflate
import hisham.keddit.common.ext.load
import hisham.keddit.news.models.RedditNews
import kotlinx.android.synthetic.main.news_item.view.*

class NewsDelegateAdapter(val itemSelectedListener: OnItemSelectedListener) : ViewTypeDelegateAdapter {

	override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder = NewsViewHolder(parent)

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder, itemType: AdapterViewType) {
		holder as NewsViewHolder
		val item = itemType as RedditNews.RedditNewsItem
		holder.bind(item)
		holder.itemView.setOnClickListener { itemSelectedListener.onItemSelected(item, holder) }
	}

	inner class NewsViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
		parent.inflate(R.layout.news_item)) {

		fun bind(item: RedditNews.RedditNewsItem) = with(itemView) {
			thumbnail.load(item.thumbnail)
			description.text = item.title
			author.text = item.author
			comments.text = "${item.numComments} comments"
			time.text = item.created.getFriendlyTime()
		}
	}

	interface OnItemSelectedListener {
		fun onItemSelected(news: RedditNews.RedditNewsItem, holder: NewsViewHolder)
	}
}