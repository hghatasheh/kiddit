package hisham.keddit.news

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import hisham.keddit.R

class NewsActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.news_activity)
		val toolbar = findViewById(R.id.toolbar) as Toolbar
		setSupportActionBar(toolbar)

		changeFragment(NewsFragment.newInstance(), false)

		val fab = findViewById(R.id.fab) as FloatingActionButton
		fab.setOnClickListener { view ->
			Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
				.setAction("Action", null).show()
		}

	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.menu_main, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		val id = item.itemId


		if (id == R.id.action_settings) {
			return true
		}

		return super.onOptionsItemSelected(item)
	}

	fun changeFragment(fragment: Fragment, cleanStack: Boolean) {
		val fragmentTransaction = supportFragmentManager.beginTransaction()

		if (cleanStack) clearBackStack()

		fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
		fragmentTransaction.replace(R.id.content_main, fragment)
		fragmentTransaction.addToBackStack(null)
		fragmentTransaction.commit()
	}

	private fun clearBackStack() {
		if (supportFragmentManager.backStackEntryCount > 1) {
			supportFragmentManager.popBackStack()
		}
	}
}
