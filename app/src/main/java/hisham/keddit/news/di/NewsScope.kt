package hisham.keddit.news.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class NewsScope