package hisham.keddit.news.di

import dagger.Component
import hisham.keddit.di.AppComponent
import hisham.keddit.news.NewsFragment

@NewsScope
@Component(modules = arrayOf(NewsModule::class), dependencies = arrayOf(AppComponent::class))
interface NewsComponent {

	fun inject(fragment: NewsFragment)
}