package hisham.keddit.news.di

import dagger.Module
import dagger.Provides
import hisham.keddit.network.RestClient
import hisham.keddit.news.NewsRepository

@Module
class NewsModule {

	@NewsScope
	@Provides
	fun newsRepository(restClient: RestClient): NewsRepository = NewsRepository(restClient.api())
}