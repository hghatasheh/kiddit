package hisham.keddit.news

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import hisham.keddit.App
import hisham.keddit.R
import hisham.keddit.base.BaseFragment
import hisham.keddit.common.ext.inflate
import hisham.keddit.common.ui.InfiniteScrollListener
import hisham.keddit.news.adapter.NewsAdapter
import hisham.keddit.news.adapter.NewsDelegateAdapter
import hisham.keddit.news.di.DaggerNewsComponent
import hisham.keddit.news.di.NewsModule
import hisham.keddit.news.models.RedditNews
import kotlinx.android.synthetic.main.news_fragment.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class NewsFragment : BaseFragment(), NewsDelegateAdapter.OnItemSelectedListener {

	@Inject
	lateinit var newsRepo: NewsRepository

	private var redditNews: RedditNews? = null

	companion object {
		fun newInstance(): NewsFragment = NewsFragment()
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		DaggerNewsComponent.builder()
			.appComponent(App.getAppComponent(this))
			.newsModule(NewsModule())
			.build()
			.inject(this)
	}

	override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return container?.inflate(R.layout.news_fragment)
	}

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		initRecyclerView()

		if (savedInstanceState == null) {
			retrieveNews()
		}
	}

	private fun addNews(news: List<RedditNews.RedditNewsItem>) {
		(recyclerView.adapter as NewsAdapter).addNews(news)
	}

	private fun retrieveNews() {
		subscriptions.add(newsRepo.getNews(redditNews?.after ?: "")
			.subscribeOn(Schedulers.io())
			.unsubscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(
				{ retrievedNews ->
					redditNews = retrievedNews
					addNews(retrievedNews.news)
				},
				{ _ -> Snackbar.make(recyclerView, R.string.error_network, Snackbar.LENGTH_SHORT).show() }
			))
	}

	private fun initRecyclerView() {
		recyclerView.apply {
			setHasFixedSize(true)
			layoutManager = LinearLayoutManager(context)
			clearOnScrollListeners()
			addOnScrollListener(InfiniteScrollListener({ retrieveNews() }, layoutManager as LinearLayoutManager))
		}
		recyclerView.adapter = NewsAdapter(this)
	}

	override fun onItemSelected(news: RedditNews.RedditNewsItem,
															holder: NewsDelegateAdapter.NewsViewHolder) {
		Toast.makeText(context, news.author, Toast.LENGTH_SHORT).show()
	}
}