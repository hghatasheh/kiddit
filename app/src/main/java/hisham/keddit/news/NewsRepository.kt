package hisham.keddit.news

import hisham.keddit.network.RedditApi
import hisham.keddit.news.models.RedditNews
import hisham.keddit.news.models.RedditNewsMock
import rx.Observable

class NewsRepository(private val api: RedditApi) {

	fun getNews(after: String = "", limit: Int = 10): Observable<RedditNews> {
		return api.getTop(after, limit)
			.map { response ->
				val data = response.data
				val news = data.children.map {
					val item = it.data
					RedditNews.RedditNewsItem(item.author,
						item.title,
						item.num_comments,
						item.createdAt,
						item.thumbnail,
						item.url)
				}

				RedditNews(data.after ?: "", data.before ?: "", news)
			}
	}

	fun getMocks(): Observable<List<RedditNews.RedditNewsItem>> {
		return Observable.defer {
			Observable.create<List<RedditNews.RedditNewsItem>> {
				subscriber ->
					if (!subscriber.isUnsubscribed)
						subscriber.onNext(RedditNewsMock.mocks())
			}
		}
	}
}