package hisham.keddit.base

import android.support.v4.app.Fragment
import rx.subscriptions.CompositeSubscription

open class BaseFragment : Fragment() {

	protected var subscriptions = CompositeSubscription()

	override fun onStart() {
		super.onStart()
		subscriptions = CompositeSubscription()
	}

	override fun onStop() {
		super.onStop()
		subscriptions.clear()
	}
}