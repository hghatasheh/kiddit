package hisham.keddit.base

interface AdapterViewType {
	fun getViewType(): Int
}

