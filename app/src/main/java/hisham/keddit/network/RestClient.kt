package hisham.keddit.network

import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RestClient {

	private val api: RedditApi

	init {
		val retrofit = Retrofit.Builder()
			.baseUrl("https://www.reddit.com/")
			.addConverterFactory(GsonConverterFactory.create())
			.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
			.build()

		api = retrofit.create(RedditApi::class.java)
	}

	fun api(): RedditApi = api
}

