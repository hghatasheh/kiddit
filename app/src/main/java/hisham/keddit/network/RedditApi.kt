package hisham.keddit.network

import hisham.keddit.news.models.RedditNewsResponse
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface RedditApi {

	@GET("top.json")
	fun getTop(@Query("after") after: String, @Query("limit") limit: Int = 10): Observable<RedditNewsResponse>
}